/* Programe Horloge Royal de Luxe
  Aiguille des heures
*/

#include <SoftwareSerial.h>
#include <Stepper.h>

SoftwareSerial serialReception (2, 3);

const int stepsPerRevolution = 300;  // Nombre de pas pour qu'une aiguille face un tour

Stepper stepper(stepsPerRevolution, 4, 5, 6, 7);


// Variable globale pour reception

int pinHeure = 8;
int pinSens = 9;
int pinCapteur = 10;

/// Variable globale pour local

int lastStatePinHeure = HIGH;

String command;
String part1;
String part2;

int stepspeed = 200;

void setup() {

  Serial.begin(9600);
  serialReception.begin(9600);

  stepper.setSpeed(100);

  pinMode(pinHeure, INPUT_PULLUP);
  pinMode(pinSens, INPUT_PULLUP);
  pinMode(pinCapteur, INPUT);

  horlogeReset (1);

  Serial.println ("fin seput");
}

void loop() {

  //***********Info aiguilleHeure*********

  int statePinHeure = digitalRead (pinHeure);
  bool sens = digitalRead (pinSens);

  if ( sens ) {
    if (statePinHeure != lastStatePinHeure) {
      stepper.step(-1);
      lastStatePinHeure = statePinHeure;
    }

  }
  else {
    if (statePinHeure != lastStatePinHeure) {
      stepper.step(1);
      lastStatePinHeure = statePinHeure;
    }

  }

  //******************Reception**********

  if (serialReception.available()) {

    char c = serialReception.read(); //get the character

    if (c == '\n') {
      //Serial.println ( command);
      parseCommand(command);
      command = "";
    }
    else {
      command += c;
    }
  }

  //********************************************************************************

  if (part1.equalsIgnoreCase("r")) {
    int val = part2.toInt();
    horlogeReset (val);
  }

  //  ********************************************************************************

  else {
    //  Serial.println("COMMAND NOT RECOGNIZED");
  }
}
